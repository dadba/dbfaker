# 前言
有时候对于不同的项目数据库中字段的生成方式多样，对于那些无法使用内置方法来实现的字段可以自己写
生成规则。下面就说下dbfaker如何加载自定义方法。

# 步骤
## 1. 初始化项目
使用命令来创建一个dbfaker项目，项目名为目录名
```shell script
dbfaker init --project_name myproject
cd myproject
```
创建成功后会在当前目录下生成一个myproject的目录，目录结构如下:
```shell script
(venv) guolong@guolong-PC:~/PycharmProjects/dbfaker-test/dbfaker-project$ tree
.
├── data
│   └── sample.yml
├── log
│   └── dbfaker.log
├── readme.md
└── script  # 自定义生成规则Python文件所在目录
    ├── __pycache__
    │   └── sample.cpython-37.pyc
    └── sample.py

4 directories, 5 files
(venv) guolong@guolong-PC:~/PycharmProjects/dbfaker-test/dbfaker-project$ ls -la
总用量 24
drwxr-xr-x 5 guolong guolong 4096 10月 19 18:10 .
drwxr-xr-x 6 guolong guolong 4096 10月 19 18:10 ..
drwxr-xr-x 2 guolong guolong 4096 10月 19 18:12 data
drwxr-xr-x 2 guolong guolong 4096 10月 19 18:10 log
-rw-r--r-- 1 guolong guolong  300 10月 19 18:10 readme.md
drwxr-xr-x 3 guolong guolong 4096 10月 19 18:10 script


```
## 2. 编写你的字段生成规则
在项目目录下的script目录下新建一个Python文件，文件名自定义（需以.py结尾），文件中添加一个继承自faker.BaseProvider的子类；类方法即为自定义的方法。
```shell script
# 示例Python方法，一个对字符串进行md5加密的方法
cat script/sample.py
```
```python
from faker.providers import BaseProvider
import hashlib


class MProvider(BaseProvider):
    def md5(self, value: bytes):
        if isinstance(value, str):
            value = value.encode()
        return hashlib.md5(value).hexdigest()

```
你可以在这个示例文件中继续添加自定义方法，也可以新起一个Python文件，继承faker.BaseProvider类即可！

## 3. 编写yaml文件，在文件中引用你写的方法
```yaml
package:
  - datetime  #　导入额外的包，在jinja2模板中使用（下面有用到datetime包，所以要先导入）

env:
  id:
    engine: faker.uuid
    rule: null
  time_format:
    engine: faker.eq
    rule:
      value: "%Y-%m-%d %H:%M:%S"

tables:
- table: stu
  comment: '学生表'
  columns:
    id:
      comment: 数据主键id
      engine: faker.eq(value='{{ env.id }}') # 通过引用环境变量中的值
    name:
      comment: 姓名
      engine: faker.name
    idcard:
      comment: 身份证号
      engine: faker.ssn
    age:
      comment: 年龄
      engine: faker.eq(value='{{ datetime.datetime.now().year - int(stu.idcard[6:10]) }}')  #　通过jinja２模板直接计算
    password:
      comment: 密码
      engine: md5('123456')  # 引用自建的md5方法
    sex:
      comment: 性别
      engine: faker.eq(value='{{ "man" if int(stu.idcard[-2]) % 2==1 else "female" }}')  #　通过jinja２模板直接计算
- table: course
  comment: '课程信息 '
  columns:
    id:
      comment: 数据主键id
      engine: faker.uuid
    stu_id:
      comment: 数据主键id
      engine: faker.eq(value='{{ stu.id }}')  # 通过其他表中的值
    course_name:
      comment: 课程名称
      engine: faker.choice(value=['数学','语文','英语','化学','地理']) # 通过内置方法从列表中随机取一个值
    course_time:
      comment: 上课时间
      engine: faker.now(format="{{ env.time_format }}")  # 通过内置方法获取当前时间，并按照指定格式返回

extraction:  # 从已生成的数据中提取字段
  stu_id:
    value: '{{ stu.id }}'
    default: null
  course_id:
    value: '{{ course.id }}'
  stu_name:
    value: '{{ stu.name }}'
    default: '测试用户'
```

## 4. 生成/插入数据库
```shell script
# 在项目目录下(script目录的所在目录)执行命令
(venv) PC:~/dbfaker-test/dbfaker-project$ dbfaker data/sample.yml -p
100%|██████████| 1/1 [00:00<00:00, 74.59组/s]
extraction data key: stu_id, value:eea756d719094e868b4d3ccfa4c8f73b
extraction data key: course_id, value:aaed113625e74cc19db48b36d03d4ea1
extraction data key: stu_name, value:雷俊
INSERT INTO `stu` (`id`,`name`,`idcard`,`age`,`sex`,`password`) VALUES ('eea756d719094e868b4d3ccfa4c8f73b','雷俊','611001199312046852','27','man','e10adc3949ba59abbe56e057f20f883e');
INSERT INTO `course` (`id`,`stu_id`,`course_name`,`course_time`) VALUES ('aaed113625e74cc19db48b36d03d4ea1','eea756d719094e868b4d3ccfa4c8f73b','语文','2020-10-27 10:38:51');
执行完成，共生成1组数据

```

